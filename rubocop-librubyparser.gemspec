# frozen_string_literal: true

require_relative "lib/rubocop/librubyparser/version"

Gem::Specification.new do |spec|
  spec.name = "rubocop-librubyparser"
  spec.version = Rubocop::Librubyparser::VERSION
  spec.authors = ["Peter Leitzen"]
  spec.email = ["pleitzen@gitlab.com"]

  spec.summary = "Use LibRubyParser for RuboCop"
  spec.description = "Patches RuboCop to use lib-ruby-parser gem instead of parser gem for speed."
  spec.homepage = "https://gitlab.com/splattael/rubocop-librubyparser"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.7.0"

  spec.metadata["allowed_push_host"] = "https://rubygems.org"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "#{spec.homepage}/-/blob/main/CHANGELOG.md"

  spec.files = Dir["lib/**/.rb"]
  spec.require_paths = ["lib"]

  spec.add_development_dependency "gitlab-styles", "~> 9.0"

  spec.add_dependency "lib-ruby-parser", "~> 4.0.3"
  spec.add_dependency "rubocop", "~> 1.36.0"
  spec.add_dependency "rubocop-rake", "~> 0.6.0"
end
