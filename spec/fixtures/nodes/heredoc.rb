### str plain
<<-HERE
  a
HERE

### str quoted
<<-'HERE'
  a
HERE

### TODO str multiline
<<-HERE
  a
  b
HERE

### TODO dstr
<<-HERE
  #{foo}
HERE

### TODO mixed
<<-HERE
  a
  #{foo}
  b
HERE
