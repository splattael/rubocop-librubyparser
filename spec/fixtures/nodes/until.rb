### oneline
until cond do; foo; end

### multiline
until cond do
  foo
end
