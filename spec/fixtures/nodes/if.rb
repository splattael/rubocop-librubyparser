### then

if true then; end

### single

if true; 1; else 2; end

### TODO: multiline

if true
  1
else
  2
end
