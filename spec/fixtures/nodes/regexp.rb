### simple
/foo/
%r{foo}

### multiline
%r{
  foo
  bar

  baz
}

### TODO multiline + dstr
%r{
  foo
  #{baz}
  bar
}
