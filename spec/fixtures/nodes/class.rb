### simple
class Foo; end

### multiline
class Foo
  1
end

### expression
class A < (obj.foo + 1); end
