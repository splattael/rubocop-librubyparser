### simple
begin; rescue; end

### match
begin; 1; rescue X, Y => e; 2; end

### else

begin; 1; rescue StandardError => e; 2; else; 3; end
