### Normal
case true
when false
when true
end

### then
case true
when false then 1
when false then 1
end

### else
case true
when false
  1
else
  2
end

### no pattern
case
when true
end
