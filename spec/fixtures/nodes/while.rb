### oneline
while cond do; foo; end

### multiline
while cond do
  foo
end
