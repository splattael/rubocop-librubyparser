### simple
case 1; in 2; end

### TODO multiline
case 1
in 2
  true
end

### then
case 1; in 2 then false; end

### TODO else
case 1
in 2
  true
else
  false
end
