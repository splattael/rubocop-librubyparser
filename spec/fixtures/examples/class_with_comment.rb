# frozen_string_literal: true
# Addon

class Foo
  # Some comment
  # Multiline
  # Multiline 2
  def default?
    [1].include?(name) # more comment inline
    # multiline
    code
  end
end

# Some post
# comments
