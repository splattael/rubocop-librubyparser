# frozen_string_literal: true

require "rubocop"

require "rubocop/librubyparser"

RuboCop::AST::Node.prepend Module.new {
  def inspect(indent = 0)
    <<~OUT
      <#{self.class}> #{super}
      #{"  " * indent}@ (#{loc.class}) #{loc&.to_hash&.pretty_inspect}"
    OUT
  end

  def ==(other)
    super && self.class == other.class && (
      (!loc && !other.loc) ||
      (loc&.to_hash&.inspect == other.loc&.to_hash&.inspect)
    )
  end
}

RuboCop::AST::Token.prepend Module.new {
  def inspect
    "[[#{line}, #{column}, #{begin_pos}..#{end_pos}], #{type}, #{text.inspect}]"
  end

  def ==(other)
    other.is_a?(RuboCop::AST::Token) &&
      pos.begin_pos == other.pos.begin_pos && pos.end_pos == other.pos.end_pos &&
      type == other.type &&
      text == other.text
  end
}

RSpec.describe Rubocop::Librubyparser do
  include Support::ParseHelper

  describe "node mapping" do
    RSpec::Matchers.define :match_ast do |expected|
      match do |actual|
        expect(actual).to eq(expected)
      end

      failure_message do |actual|
        raise "Expected AST is invalid" unless expected

        output = +"AST:\n"
        output << side_by_side(expected, actual) do |node|
          "<#{node.class}>:\n#{node}"
        end

        output << "\n"
        output << "Location:\n"
        output << side_by_side(expected.loc, actual.loc) { prettify_loc(_1) }

        output
      end

      def side_by_side(expected, actual, &block)
        expected = yield(expected).split("\n")
        actual = yield(actual).split("\n")

        expected.concat([""] * (actual.size - expected.size)) if actual.size > expected.size
        length = expected.map(&:size).max

        output = +"Expected#{" " * length}Actual\n"

        expected.zip(actual).each do |(e, a)|
          spacer = " " * (8 + length - e.size)
          output << "#{e}#{spacer}#{a}\n"
        end

        output
      end

      def prettify_loc(loc)
        return "nil" unless loc

        output = []
        output << "<#{loc.class.name.split("::").last}>:"
        loc.to_hash.each do |key, range|
          range = range ? "#{range.begin_pos}..#{range.end_pos}" : "nil"
          output << "  #{key}: #{range}"
        end

        output.join("\n")
      end

      diffable
    end

    def self.define_node_specs(files)
      files.each do |file|
        content = File.read(file)
        name = File.basename(file, ".rb")

        describe "#{name} node" do
          parts = content.split(/^###\s*(.*?)\n/)
          parts = parts.drop(1) if parts.first.empty?
          parts.unshift "" if parts.size.odd?

          parts.each_slice(2) do |(caption, source)|
            line = source.chomp.gsub(/\n/, '\n')
            caption = caption.squeeze("\n").chomp
            example_name = [caption, line].reject(&:empty?).compact.join(": ")

            it example_name do
              pending if caption.start_with?("TODO")

              source = move_source(source)
              rexpected = parse_with_rubocop(source)
              ractual = parse_with_patched(source)

              expect(ractual.ast).to match_ast(rexpected.ast)
              expect(ractual.tokens).to eq(rexpected.tokens)
            end
          end
        end
      end
    end

    def self.define_example_specs(files)
      files.each do |file|
        source = File.read(file)
        name = file

        describe "#{name} example ", :aggregate_failures do
          it "parses equivalently" do
            rexpected = parse_with_rubocop(source)
            ractual = parse_with_patched(source)

            expect(ractual.ast).to match_ast(rexpected.ast)
            expect(ractual.tokens).to eq(rexpected.tokens)
          end
        end
      end
    end

    fixtures = Dir["#{__dir__}/../fixtures/nodes/*.rb"]

    define_node_specs(fixtures)

    it "has fixtures" do
      expect(fixtures.size).to eq(123)
    end

    describe "examples" do
      examples = Dir["#{__dir__}/../fixtures/examples/*.rb"]

      it "has fixtures" do
        expect(examples.size).to eq(1)
      end

      define_example_specs(examples)

      if ENV["EXAMPLES_GLOB"]
        context "with real world examples" do
          examples = Dir[ENV["EXAMPLES_GLOB"]]

          it "has examples" do
            expect(examples.size).to be_nonzero
          end

          define_example_specs(examples)
        end
      end
    end

    context "when empty" do
      let(:content) { "" }

      specify do
        expect(parse_with_rubocop(content).ast)
          .to match_ast(parse_with_patched(content).ast)
      end
    end

    private

    def move_source(source)
      line = rand(100)
      column = rand(100)

      ["\n" * line, " " * column, source].join
    end
  end

  describe "comment mapping" do
    subject(:comments) { parse_with_patched(content).comments }

    context "with comments" do
      let(:content) { "some code # foo\n#bar" }

      specify do
        expect(comments.size).to eq(2)
        expect(comments).to be_all(kind_of(Parser::Source::Comment))
        expect(comments.map(&:text)).to contain_exactly(
          "# foo\n", "#bar"
        )
      end

      context "with UTF-8" do
        let(:content) { "some code # ♡foo\n#bar" }

        specify do
          pending "Let LibRubyParser emit different location"

          expect(comments.map(&:text)).to contain_exactly(
            "# ♡foo\n", "#bar"
          )
        end
      end
    end

    context "without comments" do
      let(:content) { "foo bar" }

      it { is_expected.to be_empty }
    end
  end

  describe "token mapping" do
    subject(:tokens) { parse_with_patched(content).tokens }

    context "when empty" do
      where(:content) do
        ["", "\n", "\n\n"]
      end

      with_them do
        it { is_expected.to be_empty }
      end
    end
  end
end
