# frozen_string_literal: true

require "rubocop"

module Support
  module ParseHelper
    class PatchedProcessedSource < RuboCop::AST::ProcessedSource
      prepend Rubocop::Librubyparser::ProcessedSourcePatch
    end

    def parse_with_patched(source)
      PatchedProcessedSource.new(source, 2.7)
    end

    def parse_with_librubyparse(source)
      LibRubyParser.parse(source, record_tokens: true)
    end

    def parse_with_rubocop(source)
      RuboCop::AST::ProcessedSource.new(source, 2.7)
    end
  end
end
