# Rubocop::Librubyparser

Use [`lib-ruby-parser`](https://rubygems.org/gems/lib-ruby-parser) in
[`rubocop`](https://rubygems.org/gems/rubocop).

## How?

This gems patches RuboCop and maps AST nodes from `lib-ruby-parser` to map
[`rubocop-ast`](https://rubygems.org/gems/rubocop-ast)'s nodes.

## Installation

Install the gem and add to the application's Gemfile by executing:

    $ bundle add rubocop-librubyparser

If bundler is not being used to manage dependencies, install the gem by
executing:

    $ gem install rubocop-librubyparser

## Usage

```ruby require "rubocop-librubyparser/patch" ```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run
`rake spec` to run the tests. You can also run `bin/console` for an interactive
prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To
release a new version, update the version number in `version.rb`, and then run
`bundle exec rake release`, which will create a git tag for the version, push
git commits and the created tag, and push the `.gem` file to
[rubygems.org](https://rubygems.org).

### Real world examples

Run specs again real source code via:

```shell
EXAMPLES_GLOB=<path_to>/gitlab/app/models/**.rb be rspec -e "real world"
```

## Contributing

Bug reports and pull requests are welcome on GitHub at
https://github.com/splattael/rubocop-librubyparser. This project is intended to
be a safe, welcoming space for collaboration, and contributors are expected to
adhere to the [code of
conduct](https://github.com/splattael/rubocop-librubyparser/blob/main/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT
License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Rubocop::Librubyparser project's codebases, issue
trackers, chat rooms and mailing lists is expected to follow the [code of
conduct](https://github.com/splattael/rubocop-librubyparser/blob/main/CODE_OF_CONDUCT.md).
