# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module BackRef
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map.new(l!(buffer, node.expression_l))
          children = [node.name.to_sym]

          RuboCop::AST::Node.new(:back_ref, children, location: location)
        end
      end
    end
  end
end
