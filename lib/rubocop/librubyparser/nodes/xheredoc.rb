# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module XHeredoc
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Heredoc.new(
            l!(buffer, node.expression_l),
            l!(buffer, node.heredoc_body_l),
            l!(buffer, node.heredoc_end_l))

          children = node.parts.map { n!(buffer, _1) }

          if children.size == 1
            str = children.first
            value = str.children.first
            multiline = value.count("\n") > 1

            if multiline
              children = value.split("\n").map do |v|
                RuboCop::AST::StrNode.new(:str, ["#{v}\n"], location: location)
              end
              RuboCop::AST::DstrNode.new(:dstr, children, location: location)
            else
              RuboCop::AST::StrNode.new(:xstr, str, location: location)
            end
          else
            RuboCop::AST::DstrNode.new(:xstr, children, location: location)
          end
        end
      end
    end
  end
end
