# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Shadowarg
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Variable.new(
            l!(buffer, node.expression_l))

          children = [node.name.to_sym]

          RuboCop::AST::ArgNode.new(:shadowarg, children, location: location)
        end
      end
    end
  end
end
