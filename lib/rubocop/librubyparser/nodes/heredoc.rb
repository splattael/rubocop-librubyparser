# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Heredoc
        extend NodeMapper

        module_function

        def map(buffer, node)
          heredoc_body = r(buffer, node.heredoc_body_l.begin, node.heredoc_end_l.begin)
          location = Parser::Source::Map::Heredoc.new(
            l!(buffer, node.expression_l),
            heredoc_body,
            l!(buffer, node.heredoc_end_l))

          children = node.parts.map { n!(buffer, _1) }

          if children.size == 1
            str = children.first
            value = str.children.first
            multiline = value.count("\n") > 1

            if multiline
              children = value.split("\n").map do |v|
                RuboCop::AST::StrNode.new(:str, ["#{v}\n"], location: location)
              end
              RuboCop::AST::DstrNode.new(:dstr, children, location: location)
            else
              RuboCop::AST::StrNode.new(:str, str, location: location)
            end
          else
            RuboCop::AST::DstrNode.new(:dstr, children, location: location)
          end
        end
      end
    end
  end
end
