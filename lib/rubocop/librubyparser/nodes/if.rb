# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module If
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Condition.new(
            l!(buffer, node.keyword_l),
            l?(buffer, node.begin_l),
            l?(buffer, node.else_l),
            l?(buffer, node.end_l),
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.cond), n?(buffer, node.if_true),
                      n?(buffer, node.if_false)]

          RuboCop::AST::IfNode.new(:if, children, location: location)
        end

        def then?(buffer, node)
          node.begin_l && lsize(node.begin_l) != 1
        end
      end
    end
  end
end
