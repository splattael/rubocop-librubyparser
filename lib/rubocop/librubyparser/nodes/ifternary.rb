# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module IfTernary
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Ternary.new(
            l!(buffer, node.question_l),
            l!(buffer, node.colon_l),
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.cond), n!(buffer, node.if_true),
                      n!(buffer, node.if_false)]

          RuboCop::AST::IfNode.new(:if, children, location: location)
        end
      end
    end
  end
end
