# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Cvasgn
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Variable.new(
            l!(buffer, node.name_l),
            l!(buffer, node.expression_l))
          location = location.with_operator(l!(buffer, node.operator_l)) if node.operator_l

          children = [node.name.to_sym, n?(buffer, node.value)]

          RuboCop::AST::AsgnNode.new(:cvasgn, children, location: location)
        end
      end
    end
  end
end
