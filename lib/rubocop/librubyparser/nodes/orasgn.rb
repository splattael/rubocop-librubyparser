# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module OrAsgn
        extend NodeMapper

        module_function

        def map(buffer, node)
          recv = n!(buffer, node.recv)
          name_l = recv.send_type? ? recv.loc.selector : recv.loc.name

          location = if node.recv.is_a?(LibRubyParser::Nodes::IndexAsgn)
                       Parser::Source::Map::Send.new(
                         nil,
                         recv.location.selector,
                         nil,
                         nil,
                         l!(buffer, node.expression_l))
                     else
                       Parser::Source::Map::Variable.new(
                         name_l,
                         l!(buffer, node.expression_l))
                     end

          location = lop!(buffer, location, node.operator_l)

          children = [recv, n!(buffer, node.value)]

          RuboCop::AST::OrAsgnNode.new(:or_asgn, children, location: location)
        end
      end
    end
  end
end
