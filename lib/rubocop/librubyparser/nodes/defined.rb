# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Defined
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Keyword.new(
            l!(buffer, node.keyword_l),
            l?(buffer, node.begin_l),
            l?(buffer, node.end_l),
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.value)]

          RuboCop::AST::DefinedNode.new(:defined?, children, location: location)
        end
      end
    end
  end
end
