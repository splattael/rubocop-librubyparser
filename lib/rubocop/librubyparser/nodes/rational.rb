# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Rational
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Operator.new(
            l?(buffer, node.operator_l),
            l!(buffer, node.expression_l))

          children = [Rational(node.value.delete("r"))]

          RuboCop::AST::Node.new(:rational, children, location: location)
        end
      end
    end
  end
end
