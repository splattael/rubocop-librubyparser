# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Class
        extend NodeMapper

        module_function

        def map(buffer, node)
          name = n!(buffer, node.name)
          location = Parser::Source::Map::Definition.new(
            l!(buffer, node.keyword_l),
            l?(buffer, node.operator_l),
            name.loc.name,
            l!(buffer, node.end_l))

          children = [name, n?(buffer, node.superclass),
                      n?(buffer, node.body)]

          RuboCop::AST::ClassNode.new(:class, children, location: location)
        end
      end
    end
  end
end
