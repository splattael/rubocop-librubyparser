# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module ArrayPatternWithTail
        extend NodeMapper

        module_function

        def map(buffer, node)
          raise "Not implemented"
        end
      end
    end
  end
end
