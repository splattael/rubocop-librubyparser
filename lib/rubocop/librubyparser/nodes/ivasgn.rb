# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Ivasgn
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Variable.new(
            l!(buffer, node.name_l),
            l!(buffer, node.expression_l))
          location = lop?(buffer, location, node.operator_l)

          children = [node.name.to_sym, n?(buffer, node.value)]

          RuboCop::AST::AsgnNode.new(:ivasgn, children, location: location)
        end
      end
    end
  end
end
