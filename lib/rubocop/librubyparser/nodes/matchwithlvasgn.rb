# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module MatchWithLvasgn
        extend NodeMapper

        module_function

        def map(buffer, node)
          raise "Implemented with Send node"
        end
      end
    end
  end
end
