# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module MatchCurrentLine
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map.new(
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.re)]

          RuboCop::AST::Node.new(:match_current_line, children, location: location)
        end
      end
    end
  end
end
