# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Float
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Operator.new(
            l?(buffer, node.operator_l),
            l!(buffer, node.expression_l))

          children = [Float(node.value)]

          RuboCop::AST::FloatNode.new(:float, children, location: location)
        end
      end
    end
  end
end
