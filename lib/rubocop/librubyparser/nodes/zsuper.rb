# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module ZSuper
        extend NodeMapper

        module_function

        def map(buffer, node)
          expression_l = l!(buffer, node.expression_l)

          location = Parser::Source::Map::Keyword.new(
            expression_l,
            nil,
            nil,
            expression_l)

          children = []

          RuboCop::AST::SuperNode.new(:zsuper, children, location: location)
        end
      end
    end
  end
end
