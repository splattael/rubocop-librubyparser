# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module SClass
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Definition.new(
            l!(buffer, node.keyword_l),
            l!(buffer, node.operator_l),
            nil,
            l!(buffer, node.end_l))

          children = [n!(buffer, node.expr), n?(buffer, node.body)]

          RuboCop::AST::SelfClassNode.new(:sclass, children, location: location)
        end
      end
    end
  end
end
