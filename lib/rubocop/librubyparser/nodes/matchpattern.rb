# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module MatchPattern
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Operator.new(
            l!(buffer, node.operator_l),
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.value), n!(buffer, node.pattern)]

          RuboCop::AST::Node.new(:match_pattern, children, location: location)
        end
      end
    end
  end
end
