# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module CSend
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Send.new(
            l!(buffer, node.dot_l), l?(buffer, node.selector_l),
            l?(buffer, node.begin_l), l?(buffer, node.end_l),
            l!(buffer, node.expression_l))
          location = location.with_operator(l!(buffer, node.operator_l)) if node.operator_l

          children = [n!(buffer, node.recv), node.method_name.to_sym]
          children.concat node.args.map { n!(buffer, _1) }

          RuboCop::AST::SendNode.new(:csend, children, location: location)
        end
      end
    end
  end
end
