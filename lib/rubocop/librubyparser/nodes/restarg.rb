# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Restarg
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Variable.new(
            l?(buffer, node.name_l),
            l!(buffer, node.expression_l))

          children = []
          children << node.name.to_sym if node.name

          RuboCop::AST::ArgNode.new(:restarg, children, location: location)
        end
      end
    end
  end
end
