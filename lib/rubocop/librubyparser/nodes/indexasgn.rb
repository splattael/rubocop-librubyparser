# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module IndexAsgn
        extend NodeMapper

        module_function

        def map(buffer, node)
          selector_l = r(buffer, node.begin_l.begin, node.end_l.end)
          location = Parser::Source::Map::Send.new(
            nil,
            selector_l,
            nil,
            nil,
            l!(buffer, node.expression_l))
          location = location.with_operator(l!(buffer, node.operator_l)) if node.operator_l

          method_name = node.value ? :[]= : :[]
          children = [n!(buffer, node.recv), method_name]
          children.concat node.indexes.map { n!(buffer, _1) }
          children << n?(buffer, node.value) if node.value

          RuboCop::AST::SendNode.new(:send, children, location: location)
        end
      end
    end
  end
end
