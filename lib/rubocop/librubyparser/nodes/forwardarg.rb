# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module ForwardArg
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map.new(
            l!(buffer, node.expression_l))

          children = []

          RuboCop::AST::ArgNode.new(:forward_arg, children, location: location)
        end
      end
    end
  end
end
