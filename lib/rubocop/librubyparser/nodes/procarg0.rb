# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Procarg0
        extend NodeMapper

        module_function

        def map(buffer, node)
          # TODO really?
          children = node.args.map { n!(buffer, _1) }

          children.first
        end
      end
    end
  end
end
