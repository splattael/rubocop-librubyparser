# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Module
        extend NodeMapper

        module_function

        def map(buffer, node)
          name = n!(buffer, node.name)
          location = Parser::Source::Map::Definition.new(
            l!(buffer, node.keyword_l),
            nil,
            name.loc.name,
            l!(buffer, node.end_l))

          children = [name, n?(buffer, node.body)]

          RuboCop::AST::ModuleNode.new(:module, children, location: location)
        end
      end
    end
  end
end
