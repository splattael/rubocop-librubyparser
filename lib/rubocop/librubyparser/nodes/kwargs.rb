# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Kwargs
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Collection.new(
            nil,
            nil,
            l!(buffer, node.expression_l))

          children = node.pairs.map { n!(buffer, _1) }

          RuboCop::AST::HashNode.new(:hash, children, location: location)
        end
      end
    end
  end
end
