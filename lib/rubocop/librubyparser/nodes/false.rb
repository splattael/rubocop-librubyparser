# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module False
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map.new(l!(buffer, node.expression_l))
          children = []

          RuboCop::AST::Node.new(:false, children, location: location) # rubocop:disable Lint/BooleanSymbol
        end
      end
    end
  end
end
