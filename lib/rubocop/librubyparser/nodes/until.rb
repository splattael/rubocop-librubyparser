# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Until
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Keyword.new(
            l!(buffer, node.keyword_l),
            l?(buffer, node.begin_l),
            l?(buffer, node.end_l),
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.cond), n?(buffer, node.body)]

          RuboCop::AST::UntilNode.new(:until, children, location: location)
        end
      end
    end
  end
end
