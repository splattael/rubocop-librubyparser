# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Send
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Send.new(
            l?(buffer, node.dot_l),
            l?(buffer, node.selector_l),
            l?(buffer, node.begin_l),
            l?(buffer, node.end_l),
            l!(buffer, node.expression_l))
          location = location.with_operator(l!(buffer, node.operator_l)) if node.operator_l

          recv = n?(buffer, node.recv)
          name = node.method_name.to_sym

          return match_with_vasgn(buffer, node, location) if name == :=~ && recv&.regexp_type?

          children = [recv, name]
          children.concat node.args.map { n!(buffer, _1) }

          RuboCop::AST::SendNode.new(:send, children, location: location)
        end

        def match_with_vasgn(buffer, node, location)
          children = [n?(buffer, node.recv)]
          children.concat node.args.map { n!(buffer, _1) }

          RuboCop::AST::Node.new(:match_with_lvasgn, children, location: location)
        end
      end
    end
  end
end
