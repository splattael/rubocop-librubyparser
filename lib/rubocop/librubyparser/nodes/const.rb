# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Const
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Constant.new(
            l?(buffer, node.double_colon_l),
            l!(buffer, node.name_l),
            l!(buffer, node.expression_l))

          children = [n?(buffer, node.scope), node.name.to_sym]

          RuboCop::AST::ConstNode.new(:const, children, location: location)
        end
      end
    end
  end
end
