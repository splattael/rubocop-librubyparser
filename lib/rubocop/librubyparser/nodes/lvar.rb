# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Lvar
        extend NodeMapper

        module_function

        def map(buffer, node)
          expression_l = l!(buffer, node.expression_l)
          location = Parser::Source::Map::Variable.new(
            expression_l,
            expression_l)

          children = [node.name.to_sym]

          RuboCop::AST::Node.new(:lvar, children, location: location)
        end
      end
    end
  end
end
