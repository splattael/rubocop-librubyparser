# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module True
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map.new(l!(buffer, node.expression_l))

          RuboCop::AST::Node.new(:true, [], location: location) # rubocop:disable Lint/BooleanSymbol
        end
      end
    end
  end
end
