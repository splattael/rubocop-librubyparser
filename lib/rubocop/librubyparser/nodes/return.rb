# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Return
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Keyword.new(
            l!(buffer, node.keyword_l),
            nil,
            nil,
            l!(buffer, node.expression_l))

          children = node.args.map { n!(buffer, _1) }

          RuboCop::AST::ReturnNode.new(:return, children, location: location)
        end
      end
    end
  end
end
