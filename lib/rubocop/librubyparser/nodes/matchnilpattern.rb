# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module MatchNilPattern
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Variable.new(
            l!(buffer, node.name_l),
            l!(buffer, node.expression_l))

          children = []

          RuboCop::AST::Node.new(:match_nil_pattern, children, location: location)
        end
      end
    end
  end
end
