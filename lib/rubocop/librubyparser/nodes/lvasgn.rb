# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Lvasgn
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Variable.new(
            l!(buffer, node.name_l),
            l!(buffer, node.expression_l))
          location = lop?(buffer, location, node.operator_l)

          children = [node.name.to_sym]
          children << n!(buffer, node.value) if node.value

          RuboCop::AST::AsgnNode.new(:lvasgn, children, location: location)
        end
      end
    end
  end
end
