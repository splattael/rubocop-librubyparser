# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module RescueBody
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::RescueBody.new(
            l!(buffer, node.keyword_l),
            l?(buffer, node.assoc_l),
            l?(buffer, node.begin_l),
            l!(buffer, node.expression_l))

          children = [n?(buffer, node.exc_list),
                      n?(buffer, node.exc_var),
                      n?(buffer, node.body)]

          RuboCop::AST::ResbodyNode.new(:resbody, children, location: location)
        end
      end
    end
  end
end
