# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module OpAsgn
        extend NodeMapper

        module_function

        def map(buffer, node)
          recv = n!(buffer, node.recv)
          name_l = recv.send_type? ? recv.loc.selector : recv.loc.name

          location = Parser::Source::Map::Variable.new(
            name_l,
            l!(buffer, node.expression_l))
          location = lop!(buffer, location, node.operator_l)

          children = [recv, node.operator.to_sym,
                      n!(buffer, node.value)]

          RuboCop::AST::OpAsgnNode.new(:op_asgn, children, location: location)
        end
      end
    end
  end
end
