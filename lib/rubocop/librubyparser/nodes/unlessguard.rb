# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module UnlessGuard
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Keyword.new(
            l!(buffer, node.keyword_l),
            nil,
            nil,
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.cond)]

          RuboCop::AST::Node.new(:unless_guard, children, location: location)
        end
      end
    end
  end
end
