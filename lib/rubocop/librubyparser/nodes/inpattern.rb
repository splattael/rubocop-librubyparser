# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module InPattern
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Keyword.new(
            l!(buffer, node.keyword_l),
            l!(buffer, node.begin_l),
            nil,
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.pattern), n?(buffer, node.guard),
                      n?(buffer, node.body)]

          RuboCop::AST::InPatternNode.new(:in_pattern, children, location: location)
        end
      end
    end
  end
end
