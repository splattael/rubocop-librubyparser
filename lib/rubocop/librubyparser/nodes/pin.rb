# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Pin
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Send.new(
            nil,
            l!(buffer, node.selector_l),
            nil,
            nil,
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.var)]

          RuboCop::AST::Node.new(:pin, children, location: location)
        end
      end
    end
  end
end
