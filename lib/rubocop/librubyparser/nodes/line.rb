# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Line
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map.new(
            l!(buffer, node.expression_l))

          children = [location.line]

          RuboCop::AST::IntNode.new(:int, children, location: location)
        end
      end
    end
  end
end
