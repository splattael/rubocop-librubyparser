# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Regexp
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Collection.new(
            l!(buffer, node.begin_l),
            l!(buffer, node.end_l),
            l!(buffer, node.expression_l))

          children = node.parts.map { n!(buffer, _1) }

          sub_expr_l = location.begin.with(begin_pos: location.begin.end_pos)

          children = children.flat_map do |child|
            next child unless child.str_type?
            next child unless child.value.include?("\n")

            child.value.split("\n").map do |value|
              value.concat("\n") unless /^\s+$/.match?(value)
              sub_expr_l = sub_expr_l.adjust(end_pos: value.bytesize)

              loc = Parser::Source::Map::Collection.new(
                nil,
                nil,
                sub_expr_l)
              sub_expr_l = sub_expr_l.with(begin_pos: sub_expr_l.end_pos)
              RuboCop::AST::StrNode.new(:str, [value], location: loc)
            end
          end

          children << (n?(buffer, node.options) || RuboCop::AST::Node.new(
            :regopt, [], location:
            Parser::Source::Map.new(r(buffer, node.end_l.end, node.end_l.end))))

          RuboCop::AST::RegexpNode.new(:regexp, children, location: location)
        end
      end
    end
  end
end
