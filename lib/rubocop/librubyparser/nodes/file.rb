# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module File
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map.new(
            l!(buffer, node.expression_l))

          children = [buffer.name]

          RuboCop::AST::StrNode.new(:str, children, location: location)
        end
      end
    end
  end
end
