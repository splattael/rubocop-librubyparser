# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Lambda
        extend NodeMapper

        module_function

        def map(buffer, node)
          expression_l = l!(buffer, node.expression_l)
          location = Parser::Source::Map::Send.new(
            nil,
            expression_l,
            nil,
            nil,
            expression_l)

          children = [nil, :lambda]

          RuboCop::AST::SendNode.new(:send, children, location: location)
        end
      end
    end
  end
end
