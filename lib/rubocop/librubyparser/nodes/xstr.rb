# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Xstr
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Collection.new(
            l!(buffer, node.begin_l),
            l!(buffer, node.end_l),
            l!(buffer, node.expression_l))

          children = node.parts.map { n!(buffer, _1) }

          RuboCop::AST::StrNode.new(:xstr, children, location: location)
        end
      end
    end
  end
end
