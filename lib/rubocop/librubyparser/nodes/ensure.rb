# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Ensure
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Condition.new(
            l!(buffer, node.keyword_l),
            nil,
            nil,
            nil,
            l!(buffer, node.expression_l))

          children = [n?(buffer, node.body), n?(buffer, node.ensure)]

          RuboCop::AST::EnsureNode.new(:ensure, children, location: location)
        end
      end
    end
  end
end
