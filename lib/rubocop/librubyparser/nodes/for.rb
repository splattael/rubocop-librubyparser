# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module For
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::For.new(
            l!(buffer, node.keyword_l),
            l!(buffer, node.operator_l),
            l!(buffer, node.begin_l),
            l!(buffer, node.end_l),
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.iterator), n!(buffer, node.iteratee),
                      n?(buffer, node.body)]

          RuboCop::AST::ForNode.new(:for, children, location: location)
        end
      end
    end
  end
end
