# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module When
        extend NodeMapper

        module_function

        def map(buffer, node)
          begin_l = l!(buffer, node.begin_l) if then?(buffer, node)
          location = Parser::Source::Map::Keyword.new(
            l!(buffer, node.keyword_l),
            begin_l,
            nil,
            l!(buffer, node.expression_l))

          children = node.patterns.map { n!(buffer, _1) }
          children << n?(buffer, node.body)

          RuboCop::AST::WhenNode.new(:when, children, location: location)
        end

        def then?(buffer, node)
          node.begin_l && lsize(node.begin_l) != 1
        end
      end
    end
  end
end
