# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Redo
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Keyword.new(
            l!(buffer, node.expression_l),
            nil,
            nil,
            l!(buffer, node.expression_l))

          children = []

          RuboCop::AST::Node.new(:redo, children, location: location)
        end
      end
    end
  end
end
