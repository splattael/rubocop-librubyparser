# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Def
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::MethodDefinition.new(
            l!(buffer, node.keyword_l),
            nil,
            l!(buffer, node.name_l),
            l?(buffer, node.end_l),
            l?(buffer, node.assignment_l),
            l!(buffer, node.expression_l))

          args = n?(buffer, node.args) || RuboCop::AST::ArgsNode
            .new(:args, [], location: Parser::Source::Map::Collection.new(nil, nil, nil))
          children = [node.name.to_sym, args, n?(buffer, node.body)]

          RuboCop::AST::DefNode.new(:def, children, location: location)
        end
      end
    end
  end
end
