# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Splat
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Operator.new(
            l!(buffer, node.operator_l),
            l!(buffer, node.expression_l))

          children = [n?(buffer, node.value)]

          RuboCop::AST::Node.new(:splat, children, location: location)
        end
      end
    end
  end
end
