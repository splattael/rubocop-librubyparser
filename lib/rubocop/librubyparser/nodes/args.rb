# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Args
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Collection.new(
            l?(buffer, node.begin_l),
            l?(buffer, node.end_l),
            l!(buffer, node.expression_l))
          children = node.args.map { n!(buffer, _1) }

          RuboCop::AST::ArgsNode.new(:args, children, location: location)
        end
      end
    end
  end
end
