# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module AndAsgn
        extend NodeMapper

        module_function

        def map(buffer, node)
          recv = n!(buffer, node.recv)
          name_l = recv.send_type? ? recv.loc.selector : recv.loc.name

          location = Parser::Source::Map::Variable.new(
            name_l,
            l!(buffer, node.expression_l)
          )
          location = location.with_operator(l!(buffer, node.operator_l))
          children = [recv, n!(buffer, node.value)]

          RuboCop::AST::AndAsgnNode.new(:and_asgn, children, location: location)
        end
      end
    end
  end
end
