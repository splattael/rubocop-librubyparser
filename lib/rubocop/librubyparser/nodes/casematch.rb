# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module CaseMatch
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Condition.new(
            l!(buffer, node.keyword_l),
            nil,
            l?(buffer, node.else_l),
            l!(buffer, node.end_l),
            l!(buffer, node.expression_l))

          children = [n!(buffer, node.expr)]
          children.concat node.in_bodies.map { n!(buffer, _1) }
          children << n?(buffer, node.else_body)

          RuboCop::AST::CaseMatchNode.new(:case_match, children, location: location)
        end
      end
    end
  end
end
