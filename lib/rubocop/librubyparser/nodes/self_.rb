# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Self_ # rubocop:disable Naming/ClassAndModuleCamelCase
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map.new(
            l!(buffer, node.expression_l))

          children = []

          RuboCop::AST::Node.new(:self, children, location: location)
        end
      end
    end
  end
end
