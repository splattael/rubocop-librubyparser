# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Rescue
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Condition.new(
            nil,
            nil,
            l?(buffer, node.else_l),
            nil,
            l!(buffer, node.expression_l))

          children = [n?(buffer, node.body)]
          children.concat node.rescue_bodies.map { n!(buffer, _1) }
          children << n?(buffer, node.else)

          RuboCop::AST::RescueNode.new(:rescue, children, location: location)
        end
      end
    end
  end
end
