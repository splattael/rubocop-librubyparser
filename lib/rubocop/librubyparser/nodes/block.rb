# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Block
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map::Collection.new(
            l!(buffer, node.begin_l),
            l!(buffer, node.end_l),
            l!(buffer, node.expression_l))
          args = n?(buffer, node.args) || RuboCop::AST::ArgsNode
            .new(:args, [], location: Parser::Source::Map::Collection.new(nil, nil, nil))
          children = [n!(buffer, node.call), args,
                      n?(buffer, node.body)]

          RuboCop::AST::BlockNode.new(:block, children, location: location)
        end
      end
    end
  end
end
