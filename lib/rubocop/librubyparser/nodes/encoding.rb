# frozen_string_literal: true

require_relative "../node_mapper"

module Rubocop
  module Librubyparser
    module Nodes
      module Encoding
        extend NodeMapper

        module_function

        def map(buffer, node)
          location = Parser::Source::Map.new(
            l!(buffer, node.expression_l))

          children = [
            RuboCop::AST::ConstNode.new(
              :const, [nil, :Encoding], location: nil),
            :UTF_8
          ]

          RuboCop::AST::ConstNode.new(:const, children, location: location)
        end
      end
    end
  end
end
