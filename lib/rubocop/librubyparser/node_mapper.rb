# frozen_string_literal: true

require "rubocop"
require "lib-ruby-parser"

module Rubocop
  module Librubyparser
    module NodeMapper
      module_function

      def map(buffer, node)
        n!(buffer, node) if node
      end

      def n!(buffer, node)
        raise "Node is nil" unless node

        MAPPING.fetch(node.class).map(buffer, node)
      end

      def n?(buffer, node)
        return unless node

        MAPPING.fetch(node.class).map(buffer, node)
      end

      def l?(buffer, location)
        return unless location

        Parser::Source::Range.new(buffer, location.begin, location.end)
      end

      def l!(buffer, location)
        raise "Location is nil" unless location

        Parser::Source::Range.new(buffer, location.begin, location.end)
      end

      def lop!(buffer, location, operator_l)
        raise "Operator is nil" unless operator_l

        location.with_operator(l!(buffer, operator_l))
      end

      def lop?(buffer, location, operator_l)
        return location unless operator_l

        location.with_operator(l!(buffer, operator_l))
      end

      def lsize(location)
        location.end - location.begin
      end

      def r(buffer, begin_, end_)
        Parser::Source::Range.new(buffer, begin_, end_)
      end

      def load_mapping
        Nodes.constants.to_h { |name| [LibRubyParser::Nodes.const_get(name), Nodes.const_get(name)] }
      end
    end
  end
end

Dir["#{__dir__}/nodes/*.rb"].sort.each do |file|
  require file
end

Rubocop::Librubyparser::NodeMapper::MAPPING =
  Rubocop::Librubyparser::NodeMapper.load_mapping.freeze
