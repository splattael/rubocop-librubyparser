# frozen_string_literal: true

module Rubocop
  module Librubyparser
    module CommentMapper
      module_function

      def map(buffer, comments)
        comments.map do |comment|
          location = comment.location
          range = Parser::Source::Range.new(buffer, location.begin, location.end)
          Parser::Source::Comment.new(range)
        end
      end
    end
  end
end
