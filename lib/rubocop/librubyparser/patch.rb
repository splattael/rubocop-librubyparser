# frozen_string_literal: true

require_relative "../librubyparser"

require "rubocop"

RuboCop::AST::ProcessedSource
  .prepend Rubocop::Librubyparser::ProcessedSourcePatch
