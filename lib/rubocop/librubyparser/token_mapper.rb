# frozen_string_literal: true

module Rubocop
  module Librubyparser
    module TokenMapper
      module_function

      def map(buffer, tokens, comments)
        tokens.pop

        comments = comments.dup
        comment = comments.shift

        mapped = []

        while token = tokens.shift
          location = token.loc
          range = Parser::Source::Range.new(buffer, location.begin, location.end)
          name = token.token_name.to_sym
          text = token.token_value

          while comment && comment.location.expression < range
            comment_range = comment.location.expression.adjust(end_pos: -1)

            if name == :tNL
              ltoken = mapped.last

              range = if ltoken.pos.line == comment_range.line
                        comment_range.end.adjust(end_pos: 1)
                      else
                        ltoken.pos.end.adjust(end_pos: 1)
                      end
            end

            mapped << t(comment_range, :tCOMMENT, comment.text.chomp)
            comment = comments.shift
          end

          case name
          when :tSYMBEG
            if %w[tIDENTIFIER tCONSTANT tFID].include?(tokens[0].token_name)
              name = :tSYMBOL
              text = tokens.shift.token_value
              range = range.adjust(end_pos: text.size)
            end
          when :tOP_ASGN
            text = text.delete("=")
          when :tNTH_REF
            text = text.delete("$")
          when :tINTEGER
            text = text.delete("_")
          when :tUMINUS_NUM
            name = :tUNARY_NUM
          when :tNL
            text = ""
          when :tSPACE
            text = ""
            range = :skip if mapped.last&.type == :tQWORDS_BEG
          when :tSTRING_CONTENT
            if text.include?("\n")
              text.split("\n").each do |value|
                range = range.with(end_pos: range.begin_pos + value.size + 1)
                mapped << t(range, name, value.concat("\n"))
                range = range.end
              end

              range = :skip
            end
          when :kDEF
            if tokens[0].token_name == "tFID"
              tokens[0].instance_variable_set(:@token_name, "tIDENTIFIER")
            end
          when :tLABEL_END
            text.sub!(/:$/, "")
          when :tSTRING_BEG
            if (text.start_with?("'") || text.start_with?('"')) &&
                tokens[1].token_name == 'tLABEL_END' && tokens[1].token_value.end_with?(":")
            elsif text.start_with?("<<")
              quote = text.include?("'") ? "'" : '"'
              text = "<<#{quote}"
            elsif tokens[0].token_name == "tSTRING_CONTENT"
              name = :tSTRING
              text = tokens.shift.token_value
              range = range.adjust(end_pos: text.size + 1)
              tokens.shift
            end
          when :tREGEXP_END
            matched = /^(.)(\w+)?/.match(text)
            text = matched[2] || ""
            mapped << t(range.adjust(end_pos: -text.size), :tSTRING_END, matched[1])
            name = :tREGEXP_OPT
            range = range.adjust(begin_pos: 1)
          when :tRATIONAL
            text = text.to_r
          when :tIMAGINARY
            text = text.to_c
          end

          mapped << t(range, name, text) unless :skip == range
        end

        while comment
          mapped << t(comment.location.expression.adjust(end_pos: -1), :tCOMMENT, comment.text.chomp)
          comment = comments.shift
        end

        mapped
      end

      def t(range, name, text)
        RuboCop::AST::Token.new(range, name, text)
      end
    end
  end
end
