# frozen_string_literal: true

require_relative "librubyparser/version"
require_relative "librubyparser/comment_mapper"
require_relative "librubyparser/node_mapper"
require_relative "librubyparser/token_mapper"

require "lib-ruby-parser"

module Rubocop
  module Librubyparser
    module ProcessedSourcePatch
      def tokenize(parser)
        result = LibRubyParser.parse(@buffer.source, record_tokens: true)

        result.ast = NodeMapper.map(@buffer, result.ast)
        result.ast&.complete!

        result.comments = CommentMapper.map(@buffer, result.comments)

        result.tokens = TokenMapper.map(@buffer, result.tokens, result.comments)

        [result.ast, result.comments, result.tokens]
      end

      def create_parser(_)
        # noop
      end
    end
  end
end
